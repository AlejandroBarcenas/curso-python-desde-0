# Curso Python desde 0

## Instrucciones
1. Clona el repositorio por el método HTTPS
2. Crea una nueva rama con tu nombre y apellido (Ej. JuanPerez)
3. Crea un nuevo archivo llamado **datos_personales.txt** con el siguiente contenido:
    1. Nombre: Escribe tu nombre (Ej. Juan Pérez Lopez)
    2. Nivel de estudios: Escribe tu último nivel de estudios (Ej. Universidad)
    3. Escuela: Escribe la escuela de tu último nivel de estudios (Ej. Escuela Superior de Cómputo (IPN) )
    4. Título: Escribe tu título obtenido (Ej. Estudiante, Ingeniero en Sistemas Computacionales, trunco, etc.)
4. Sube tus cambios al repositorio
5. Has terminado tu práctica de Git
